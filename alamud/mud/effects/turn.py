# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .effect import Effect2, Effect3
from mud.events import TurnOnEvent, TurnOffEvent, TurnWithEvent

class TurnOnEffect(Effect2):
    EVENT = TurnOnEvent

class TurnOffEffect(Effect3):
    EVENT = TurnOffEvent

class TurnWithEffect(Effect3):
    EVENT = TurnWithEvent
