# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .event import Event2, Event3

class TurnOnEvent(Event2):
    NAME = "turn-on"

    def perform(self):
        if not self.object.has_prop("auto-igniteur") and not self.object.has_prop("lightable"):
            self.fail()
            return self.inform("turn-on.failed")
        self.inform("turn-on")


class TurnOffEvent(Event2):
    NAME = "turn-off"

    def perform(self):
        if (not self.object.has_prop("inflammable") and not self.object.has_prop("lightable") ):
            self.fail()
            return self.inform("turn-off.failed")
        if (not self.object.has_prop("light-on") and not self.object.has_prop("igniteur")):
            self.fail()
            return self.inform("turn-off.failed")
        self.inform("turn-off")
        
class TurnWithEvent(Event3):
	NAME = "turn-on-with"
	
	def perform(self):
		if not self.object.has_prop("inflammable"):
			self.fail()
			return self.inform("turn-on-with.failed")
		if not self.object2.has_prop("igniteur"):
			self.fail()
			return self.inform("turn-on-with.failed")
		self.inform("turn-on-with")
