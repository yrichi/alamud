# -*- coding: utf-8 -*-
# Copyright (C) 2014 Denys Duchier, IUT d'Orléans
#==============================================================================

from .action import Action2, Action3
from mud.events import TurnOnEvent, TurnOffEvent, TurnWithEvent

class TurnOnAction(Action2):
    EVENT = TurnOnEvent
    ACTION = "turn-on"
    RESOLVE_OBJECT = "resolve_for_use"

class TurnOffAction(Action2):
    EVENT = TurnOffEvent
    ACTION = "turn-off"
    RESOLVE_OBJECT = "resolve_for_use"

class TurnWithAction(Action3):
    EVENT = TurnWithEvent
    ACTION = "turn-on-with"
    RESOLVE_OBJECT = "resolve_for_operate"
    RESOLVE_OBJECT2 = "resolve_for_use"
