Alice:
======
Réalisation d'un item de backlog  par Alice :
----------------------------------------------------- 
- Alice prend un item de backlog et implémente le test correspondant
- Alice fait les tests, constate que ça échoue
- git commit ...
- implémente le code nécéssaire pour que le teste passe
- Reteste
- git commit -am "Item machin réalisé"

Alice pousse son master sur son remote:
---------------------------------------
 - git push origin master
 - ou simplement : git push

Bob:
====
Vérifie qu'il a bien 2 remotes:
-------------------------------
 - Le sien : origin
 - celui d'Alice

Récupère le master d'Alice:
----------------------------
 - git fetch Alice master

éventuellement consulte la branche locale correspondant au master d'Alice:
-------------------------------------------
 - git branch -av
 - git chechout Alice/master

revient dans son master:
-----------------------
 - git checkout master

puis merge le travail d'Alice et pousse les modifs dans son dépôt distant:
---------------------------------------------------
 - git merge Alice/master
 - git push
 - detruit la branche locale d'Alice :
 	- branch -d Alice/master

Alice:
======
- fetche le master de Bob pour se mettre à jour: 
	- git fetch Bob master
- Fusionne:
	- git merge Bob/master

